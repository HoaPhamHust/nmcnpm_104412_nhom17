<%@page import="java.util.ArrayList"%>
<%@page import="com.nhom17.model.dto.Phim"%>
<%@page import="java.util.List"%>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	%>
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
	<jsp:include page="/WEB-INF/templates/web/include_header_resources.jsp" />
	<jsp:include page="/WEB-INF/templates/web/include_header_resources2.jsp" />
	<%
		List<Phim> nowShowingMovieList = (ArrayList<Phim>) request.getAttribute("Now_Showing_Movie_List");
		Phim hotMovie = nowShowingMovieList.get(0);
		String url2 = hotMovie.getPoster2URL();
		String dienvien[] = hotMovie.getDienVien().split(",");
		StringBuilder new_dienvien = new StringBuilder();
		if (dienvien.length > 2) {
			new_dienvien.append(dienvien[0]);
			new_dienvien.append(", ");
			new_dienvien.append(dienvien[1]);
		} else {
			new_dienvien = new StringBuilder(hotMovie.getDienVien());
		}

		String url = request.getContextPath() + "/movie?id=" + hotMovie.getMaPhim();

		url2 = url2 == null? "https://newsini.com/upload/news/image_1538940748_7166322.jpg" : url2;

	%>
<style type="text/css">
.header {
	background:
		url(<%=url2%>)
		no-repeat 0px 0px;
	background-size: cover;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	-ms-background-size: cover;
	max-height: 750px;
}
</style>
</head>
<body>
	<div class="full">
		<jsp:include page="/WEB-INF/templates/web/menu.jsp"/>
		<div class="main">
			<div class="header">
				<!-- includes header file here -->
				<jsp:include page="/WEB-INF/templates/web/header.jsp" />
				<div class="header-info">

					<h1><%=hotMovie.getTenPhim()%></h1>
					<p class="age">
						<a href="#"><%=hotMovie.getNhanPhim()%></a> <%=new_dienvien.toString()%>
					</p>
					<p class="review">Rating
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp; <%=hotMovie.getImdbRank()%>/10</p>
					<p class="review reviewgo">Genre
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;Action</p>
					<p class="review">Release &nbsp;&nbsp;&nbsp;&nbsp;:
						&nbsp;&nbsp; <%=hotMovie.getNgayBatDau().format(DateTimeFormatter.ofPattern("dd MMM, yyyy"))%></p>
					<p class="special"><%=hotMovie.getMotaPhim()%></p>
					<a class="book" href="<%=url%>"><i class="book1"></i>BOOK TICKET</a>
				</div>
			</div>
			<h2 class="page-heading" style="margin-top: 50px;">Now Showing Movies</h2>
			<jsp:include page="/WEB-INF/templates/web/slider1.jsp" />
			<h2 class="page-heading" style="margin-top: 50px;">Comming Up Movies</h2>
			<jsp:include page="/WEB-INF/templates/web/slider2.jsp" />
			<jsp:include page="/WEB-INF/templates/web/footer.jsp" />
		</div>
	</div>
	<div class="clearfix"></div>
</body>
<script type="text/javascript">
	$(document).ready(function($) {
	});
	function sliderImageClick(url) {
		window.location.href = url;
	};
</script>
</html>