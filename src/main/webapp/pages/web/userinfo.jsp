<%@ page import="com.nhom17.model.dto.ThanhVien" %>
<%@ page import="com.nhom17.util.AppUtils" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Signup</title>
	<jsp:include page="/WEB-INF/templates/web/include_header_resources.jsp" />
	<link rel="stylesheet" href="/resources/css/signup-style.css" type="text/css" />
	<link rel="stylesheet" href="/resources/css/style3860.css" type="text/css" />
</head>
<body>
<script type="text/javascript">
    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
    });
</script>
<div class="full">
	<jsp:include page="/WEB-INF/templates/web/menu.jsp"/>
	<div class="main">
		<div class="error-content">
			<jsp:include page="/WEB-INF/templates/web/header.jsp" />
			<%
				ThanhVien thanhVien = AppUtils.getLoginedUser(request.getSession(false));
				String fullName = thanhVien.getHoTen();
				String email = thanhVien.getEmail();
				String address = thanhVien.getDiaChi();
				String phone = thanhVien.getSoDienThoai();
				int sex = thanhVien.getGioiTinh();
				String gender = "Male";
				if (sex == 1) {
					gender = "Female";
				} else if (sex == 2) {
					gender = "Other";
				}

				String birthday = thanhVien.getNgaySinh().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
			%>
			<div class="container">
				<h2 class="page-heading" style="margin-top: 50px;">User Info</h2>
				<hr>
				<div class="row">
					<!-- left column -->
					<div class="col-md-3">
						<div class="text-center">
							<img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
						</div>
					</div>

					<!-- edit form column -->
					<div class="col-md-9 personal-info">
						<h2 class="page-heading" style="margin-top: 20px; font-size: 14px;">Personal Info</h2>
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-lg-3 control-label">Full name:</label>
								<div class="col-lg-8 form-control-static">
									<label><%=fullName%></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Email:</label>
								<div class="col-lg-8 form-control-static">
									<label><%=email%></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Address:</label>
								<div class="col-lg-8 form-control-static">
									<label><%=address%></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Birthday:</label>
								<div class="col-lg-8 form-control-static">
									<label><%=birthday%></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Gender:</label>
								<div class="col-lg-8 form-control-static">
									<label><%=gender%></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Phone:</label>
								<div class="col-lg-8 form-control-static">
									<label><%=phone%></label>
								</div>
							</div>
						</form>
						<div class="form-group">
							<button type="submit" class="btn btn-info" data-toggle="modal" data-target="#edit">
								<span class="glyphicon glyphicon-edit"></span> Edit
							</button>

						</div>
						<div class="modal fade" id="edit" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<%--<button type="button" class="" data-dismiss="modal">&times;</button>--%>
										<h3 class="text-primary">Personal Info</h3>
									</div>
									<div class="modal-body">
										<div class="row">
											<form id="edit-form" method="post" action="/ajax/info" role="form" class="form-horizontal">
												<div class="form-group">
													<label class="control-label col-md-3">Full Name:</label>
													<div class="col-md-9">
														<input type="text" class="form-control" name="name" value="<%=fullName%>" required>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Email address:</label>
													<div class="col-md-9">
														<input type="email" class="form-control" name="email" value="<%=email%>" required>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Address:</label>
													<div class="col-md-9">
														<input type="text" class="form-control" name="address" value="<%=address%>" required>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Birthday:</label>
													<div class="col-md-9">
														<div class="input-group date" data-provide="datepicker">
															<input type="text" name="birthday" class="form-control" value="<%=birthday%>">
															<div class="input-group-addon">
																<span class="glyphicon glyphicon-th"></span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Gender:</label>
													<div class="col-md-9">
														<select id="gender" name="gender" class="form-control">
															<option value="0" selected>Male</option>
															<option value="1">Female</option>
															<option value="2">Other</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Phone:</label>
													<div class="col-md-9">
														<input type="text" class="form-control" name="phone" value="<%=phone%>" required>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-3"></div>
													<div class="col-md-9">
														<input type="submit" name="" value="Submit" class="btn btn-info">
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<jsp:include page="/WEB-INF/templates/web/footer.jsp" />
	</div>
</div>
<div class="clearfix"></div>
<jsp:include page="/WEB-INF/templates/web/include_body_resources.jsp"/>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/alert.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/userinfo.js"></script>

</body>
</html>