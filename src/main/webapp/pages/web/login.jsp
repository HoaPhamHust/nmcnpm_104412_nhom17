<%@ page language="java" contentType="text/html; charset=UTF-8"
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sign in</title>
    <jsp:include page="/WEB-INF/templates/web/include_header_resources.jsp"/>
    <link rel="stylesheet" href="/resources/css/signup-style.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/css/style3860.css" type="text/css" />
</head>
<body>
<div class="full">
    <jsp:include page="/WEB-INF/templates/web/menu.jsp"/>
    <div class="main">
        <div class="error-content">
            <jsp:include page="/WEB-INF/templates/web/header.jsp"/>

            <div class="signup-form-container">

                <!-- form start -->
                <form role="form" id="register-form" autocomplete="off" >

                    <div class="form-header">
                        <h2 class="page-heading">Sign in</h2>

                    </div>

                    <input type="hidden" name="redirectId" value="${param.redirectId}" />

                    <div class="form-body">

                        <div class="alert alert-info" id="message" style="display:none;">
                            submitted
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                <input name="username" type="text" class="form-control" placeholder="Username or Email">
                            </div>
                            <span class="help-block" id="error"></span>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                            <input name="pass" id="password" type="password" class="form-control"
                                   placeholder="Password">
                        </div>
                        <span class="help-block" id="error"></span>


                    </div>

                    <div class="form-footer">

                        <button type="submit" class="btn btn-info btn-block center-block">
                            <span class="glyphicon glyphicon-log-in"></span> Sign In!
                        </button>
                        <div class="text-center" style="margin-top: 10px;"><a href = "/signup">Create account</a><span> or </span><a href = "#">reset password</a></div>
                    </div>


                </form>

            </div>


        </div>
        <jsp:include page="/WEB-INF/templates/web/footer.jsp"/>
    </div>
</div>
<div class="clearfix"></div>
<jsp:include page="/WEB-INF/templates/web/include_body_resources.jsp"/>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/alert.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/signin.js"></script>


</body>
</html>