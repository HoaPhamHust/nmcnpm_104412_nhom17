<%-- 
    Document   : ThongKe
    Created on : Dec 18, 2018, 7:11:26 PM
    Author     : letha
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.nhom17.model.dto.ThongKe"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Admin home</title>

        <meta name="description" content="responsive photo gallery using colorbox" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="resources/backend/assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="resources/backend/assets/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="resources/backend/assets/css/colorbox.min.css" />

        <!-- text fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="resources/backend/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="resources/backend/assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="resources/backend/assets/css/ace-rtl.min.css" />

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="resources/backend/assets/js/ace-extra.min.js"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="no-skin">	
        
        <!--begin header-->
        <jsp:include page="HeaderAdmin.jsp"/>

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.loadState('main-container')
                } catch (e) {
                }
            </script>

            <!-- Begin menu -->
            <jsp:include page="MenuBar.jsp"/>
            <!-- End menu -->


            <!-- Begin Content -->
            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="admin">Home</a>
                            </li>

                            <li class="active">
                                <% String ngayChieu = (String)request.getAttribute("ngayChieu");%>
                                <a href="ThongKeByDateControllerServlet">Thống kê theo <%=ngayChieu%></a>
                            </li>


                        </ul><!-- /.breadcrumb -->

                    </div>
                    <div class="page-content">

                        <div class="row">
                            <div class="col-xs-12">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <table id="simple-table" class="table  table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    
                                                    <th class="center">
                                                        Tên phim
                                                    </th>
                                                    <th class="center">
                                                        Số lượng xuất chiếu
                                                    </th>
                                                    <th class="center">
                                                        Số lượng ghế cung cấp
                                                    </th>
                                                    <th class="center">
                                                        Số lượng ghế đã bán
                                                    </th>
                                                    <th class="center">
                                                        Tổng thu
                                                    </th>
                                                    
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <%
                                                    ArrayList<ThongKe> listThongKe = (ArrayList<ThongKe>) request.getAttribute("ListThongKe");
                                                    if (listThongKe != null && !listThongKe.isEmpty()) {
                                                        Iterator<ThongKe> iterator = listThongKe.iterator();
                                                        while (iterator.hasNext()) {
                                                            ThongKe tk = iterator.next();
                                                %>

                                                <tr>
                                                    
                                                    <td class="center">
                                                        <p><%=tk.getTenPhim()%></p>
                                                        
                                                        
                                                    </td>
                                                    <td class="center">
                                                        <p><%=tk.getSoLuongXuatChieu()%></p>
                                                    </td>
                                                    <td class="center">
                                                        <p><%=tk.getSoLuongGheCungCap()%></p>
                                                    </td>
                                                    <td class="center">
                                                        <p><%=tk.getSoLuongGheDaBan()%></p>
                                                    </td>
                                                    
                                                    <td class="center">
                                                        <p><%=tk.getTongThu()%></p>
                                                    </td>
                                                    
                                                </tr>
                                                <%
                                                    }
}
                                                %>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div>
                                            <ul class="pagination">

                                                <li class="active"><a href="#">Prev</a></li>

                                                <li><a href="#">Next</a></li>




                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                



                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.main-content -->
            <!-- End Content -->
            <!--Begin Footer-->
            
            <jsp:include page="FooterAdmin.jsp"/>
            
            <!--End footer-->
          

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <script src="resources/backend//myjs/jquery-2.1.4.min.js.download"></script>

        <script type="text/javascript">
                            if ('ontouchstart' in document.documentElement)
                                document.write("<script src='Template/Backend/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="resources/backend/myjs/bootstrap.min.js.download"></script>

        <!-- page specific plugin scripts -->
        <script src="resources/backend/myjs/jquery.dataTables.min.js.download"></script>
        <script src="resources/backend//myjs/jquery.dataTables.bootstrap.min.js.download"></script>
        <script src="resources/backend//myjs/dataTables.buttons.min.js.download"></script>
        <script src="resources/backend//myjs/buttons.flash.min.js.download"></script>
        <script src="resources/backend//myjs/buttons.html5.min.js.download"></script>
        <script src="resources/backend//myjs/buttons.print.min.js.download"></script>
        <script src="resources/backend//myjs/buttons.colVis.min.js.download"></script>
        <script src="resources/backend//myjs/dataTables.select.min.js.download"></script>


        <script src="resources/backend//myjs/ace-elements.min.js.download"></script>
        <script src="resources/backend//myjs/ace.min.js.download"></script>


        <script type="text/javascript">
                            jQuery(function ($) {
                                //initiate dataTables plugin
                                var myTable =
                                        $('#dynamic-table')
                                        //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                                        .DataTable({
                                            bAutoWidth: false,
                                            "aoColumns": [
                                                {"bSortable": false},
                                                null, null, null, null, null,
                                                {"bSortable": false}
                                            ],
                                            "aaSorting": [],

                                            select: {
                                                style: 'multi'
                                            }
                                        });



                                $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                                new $.fn.dataTable.Buttons(myTable, {
                                    buttons: [
                                        {
                                            "extend": "colvis",
                                            "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                                            "className": "btn btn-white btn-primary btn-bold",
                                            columns: ':not(:first):not(:last)'
                                        },
                                        {
                                            "extend": "copy",
                                            "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                                            "className": "btn btn-white btn-primary btn-bold"
                                        },
                                        {
                                            "extend": "csv",
                                            "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                                            "className": "btn btn-white btn-primary btn-bold"
                                        },
                                        {
                                            "extend": "excel",
                                            "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                                            "className": "btn btn-white btn-primary btn-bold"
                                        },
                                        {
                                            "extend": "pdf",
                                            "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                                            "className": "btn btn-white btn-primary btn-bold"
                                        },
                                        {
                                            "extend": "print",
                                            "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                                            "className": "btn btn-white btn-primary btn-bold",
                                            autoPrint: false,
                                            message: 'This print was produced using the Print button for DataTables'
                                        }
                                    ]
                                });
                                myTable.buttons().container().appendTo($('.tableTools-container'));

                                //style the message box
                                var defaultCopyAction = myTable.button(1).action();
                                myTable.button(1).action(function (e, dt, button, config) {
                                    defaultCopyAction(e, dt, button, config);
                                    $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
                                });


                                var defaultColvisAction = myTable.button(0).action();
                                myTable.button(0).action(function (e, dt, button, config) {

                                    defaultColvisAction(e, dt, button, config);


                                    if ($('.dt-button-collection > .dropdown-menu').length == 0) {
                                        $('.dt-button-collection')
                                                .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
                                                .find('a').attr('href', '#').wrap("<li />")
                                    }
                                    $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
                                });

                                ////

                                setTimeout(function () {
                                    $($('.tableTools-container')).find('a.dt-button').each(function () {
                                        var div = $(this).find(' > div').first();
                                        if (div.length == 1)
                                            div.tooltip({container: 'body', title: div.parent().text()});
                                        else
                                            $(this).tooltip({container: 'body', title: $(this).text()});
                                    });
                                }, 500);





                                myTable.on('select', function (e, dt, type, index) {
                                    if (type === 'row') {
                                        $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
                                    }
                                });
                                myTable.on('deselect', function (e, dt, type, index) {
                                    if (type === 'row') {
                                        $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
                                    }
                                });




                                /////////////////////////////////
                                //table checkboxes
                                $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

                                //select/deselect all rows according to table header checkbox
                                $('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function () {
                                    var th_checked = this.checked;//checkbox inside "TH" table header

                                    $('#dynamic-table').find('tbody > tr').each(function () {
                                        var row = this;
                                        if (th_checked)
                                            myTable.row(row).select();
                                        else
                                            myTable.row(row).deselect();
                                    });
                                });

                                //select/deselect a row when the checkbox is checked/unchecked
                                $('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
                                    var row = $(this).closest('tr').get(0);
                                    if (this.checked)
                                        myTable.row(row).deselect();
                                    else
                                        myTable.row(row).select();
                                });



                                $(document).on('click', '#dynamic-table .dropdown-toggle', function (e) {
                                    e.stopImmediatePropagation();
                                    e.stopPropagation();
                                    e.preventDefault();
                                });



                                //And for the first simple table, which doesn't have TableTools or dataTables
                                //select/deselect all rows according to table header checkbox
                                var active_class = 'active';
                                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
                                    var th_checked = this.checked;//checkbox inside "TH" table header

                                    $(this).closest('table').find('tbody > tr').each(function () {
                                        var row = this;
                                        if (th_checked)
                                            $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                                        else
                                            $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                                    });
                                });

                                //select/deselect a row when the checkbox is checked/unchecked
                                $('#simple-table').on('click', 'td input[type=checkbox]', function () {
                                    var $row = $(this).closest('tr');
                                    if ($row.is('.detail-row '))
                                        return;
                                    if (this.checked)
                                        $row.addClass(active_class);
                                    else
                                        $row.removeClass(active_class);
                                });



                                /********************************/
                                //add tooltip for small view action buttons in dropdown menu
                                $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

                                //tooltip placement on right or left
                                function tooltip_placement(context, source) {
                                    var $source = $(source);
                                    var $parent = $source.closest('table')
                                    var off1 = $parent.offset();
                                    var w1 = $parent.width();

                                    var off2 = $source.offset();
                                    //var w2 = $source.width();

                                    if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                                        return 'right';
                                    return 'left';
                                }




                                /***************/
                                $('.show-details-btn').on('click', function (e) {
                                    e.preventDefault();
                                    $(this).closest('tr').next().toggleClass('open');
                                    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
                                });

                            })
        </script>

    </body>
</html>
