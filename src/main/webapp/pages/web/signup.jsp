<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Signup</title>
	<jsp:include page="/WEB-INF/templates/web/include_header_resources.jsp" />
	<link rel="stylesheet" href="/resources/css/signup-style.css" type="text/css" />
	<link rel="stylesheet" href="/resources/css/style3860.css" type="text/css" />
</head>
<body>
<div class="full">
	<jsp:include page="/WEB-INF/templates/web/menu.jsp"/>
	<div class="main">
		<div class="error-content">
			<jsp:include page="/WEB-INF/templates/web/header.jsp" />

			<div class="signup-form-container">

				<!-- form start -->
				<form role="form" id="register-form" autocomplete="off" action="#">

					<div class="form-header">
						<h3 class="form-title"><i class="fa fa-user"></i> Sign Up</h3>

					</div>

					<div class="form-body">

						<div class="alert alert-info" id="message" style="display:none;">
							submitted
						</div>

						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><span class="glyphicon glyphicon-credit-card"></span></div>
								<input name="fullname" type="text" class="form-control" placeholder="Full Name">
							</div>
							<span class="help-block" id="error"></span>
						</div>

						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
								<input name="username" type="text" class="form-control" placeholder="Username">
							</div>
							<span class="help-block" id="error"></span>
						</div>

						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
								<input name="email" type="text" class="form-control" placeholder="Email">
							</div>
							<span class="help-block" id="error"></span>
						</div>

						<div class="row">

							<div class="form-group col-lg-6">
								<div class="input-group">
									<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
									<input name="password" id="password" type="password" class="form-control" placeholder="Password">
								</div>
								<span class="help-block" id="error"></span>
							</div>

							<div class="form-group col-lg-6">
								<div class="input-group">
									<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
									<input name="cpassword" type="password" class="form-control" placeholder="Retype Password">
								</div>
								<span class="help-block" id="error"></span>
							</div>

						</div>


					</div>

					<div class="form-footer">
						<button type="submit" class="btn btn-info">
							<span class="glyphicon glyphicon-log-in"></span> Sign Up!
						</button>
					</div>


				</form>

			</div>



		</div>
		<jsp:include page="/WEB-INF/templates/web/footer.jsp" />
	</div>
</div>
<div class="clearfix"></div>
<jsp:include page="/WEB-INF/templates/web/include_body_resources.jsp"/>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/alert.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/signup.js"></script>

</body>
</html>