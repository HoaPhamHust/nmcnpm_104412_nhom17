$('document').ready(function () {
    // name validation
    var nameregex = /^[a-zA-Z0-9]+$/;

    $.validator.addMethod("validname", function (value, element) {
        return this.optional(element) || nameregex.test(value);
    });

    $.validator.addMethod("checkAvailable", function (value, element) {
        var isSuccess = false;

        $.ajax({
            url: '/ajax/user',
            dataType: 'json',
            async: false,
            data: {username: value},
            success: function (data) {
                console.log(data["status"])
                isSuccess = data["status"] == "true";
            }
        });
        return isSuccess;
    });

    $.validator.addMethod("checkEmailAvailable", function (value, element) {
        var isSuccess = false;

        $.ajax({
            url: '/ajax/user',
            dataType: 'json',
            async: false,
            data: {email: value},
            success: function (data) {
                console.log(data["status"])
                isSuccess = data["status"] == "true";
            }
        });
        return isSuccess;
    });

    // valid email pattern
    var eregex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    $.validator.addMethod("validemail", function (value, element) {
        return this.optional(element) || eregex.test(value);
    });

    $("#register-form").submit(function (e) {
        e.preventDefault();
    }).validate({

        rules:
            {
                fullname: {
                    required: true
                },
                username: {
                    required: true,
                    validname: true,
                    minlength: 4,
                    // checkAvailable: true
                    remote: "/ajax/user"
                },
                email: {
                    required: true,
                    validemail: true,
                    remote: "/ajax/user"

                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 15
                },
                cpassword: {
                    required: true,
                    equalTo: '#password'
                },
            },
        messages:
            {
                username: {
                    required: "Please Enter User Name",
                    validname: "Name must contain only alphabets and space",
                    minlength: "Your Name is Too Short",
                    remote: "Your username has been used"
                },
                email: {
                    required: "Please Enter Email Address",
                    validemail: "Enter Valid Email Address",
                    remote: "Your email has been used"
                },
                password: {
                    required: "Please Enter Password",
                    minlength: "Password at least have 8 characters"
                },
                cpassword: {
                    required: "Please Retype your Password",
                    equalTo: "Password Did not Match !"
                }
            },
        errorPlacement: function (error, element) {
            $(element).closest('.form-group').find('.help-block').html(error.html());
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.help-block').html('');
        },

        submitHandler: function (form) {

            var data = $(form).serialize();
            console.log(data);
            $.ajax({
                url: "/signup",
                method: 'post',
                data: data,
                success: function (data) {
                    var json = JSON.parse(data);
                    if (json.status == "true") {
                        $.alert('Sign up successfully!', {
                            autoClose: true,
                            closeTime: 3000,
                            withTime: false,
                            type: 'success',
                            position: ['top-right', [0, 0]],
                            speed: 'normal',
                            isOnly: true,
                            onClose: function () {
                                window.location.href = "/login"
                            }
                        });

                    } else {
                        $.alert('Something failed, please try again!', {
                            autoClose: true,
                            closeTime: 3000,
                            withTime: false,
                            type: 'danger',
                            position: ['top-right', [0, 0]],
                            speed: 'normal',
                            isOnly: true,
                            onClose: function () {

                            }
                        });
                    }
                }
            })
            //var url = $('#register-form').attr('action');
            //location.href=url;

        }

        /*submitHandler: function()
                       {
                               alert("Submitted!");
                            $("#register-form").resetForm();
                       }*/

    });


    /*function submitForm(){


        /*$('#message').slideDown(200, function(){

            $('#message').delay(3000).slideUp(100);
            $("#register-form")[0].reset();
            $(element).closest('.form-group').find("error").removeClass("has-success");

        });

        alert('form submitted...');
        $("#register-form").resetForm();

    }*/
});