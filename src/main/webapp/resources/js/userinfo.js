$('document').ready(function()
{
    // valid email pattern
    var eregex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    $.validator.addMethod("validemail", function (value, element) {
        return this.optional(element) || eregex.test(value);
    });

    $("#edit-form").submit(function (e) {
        e.preventDefault();
    }).validate({
        rules:
            {
                email: {
                    required: true,
                    validemail: true
                }
            },
        messages:
            {
                email: {
                    required: "Please Enter Email Address",
                    validemail: "Enter Valid Email Address"
                }
            },

        errorPlacement : function(error, element) {
            $(element).closest('.form-group').find('.help-block').html(error.html());
        },
        highlight : function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.help-block').html('');
        },

        submitHandler: function(form){

            var data = $(form).serialize();

            $.ajax({
                url: "/ajax/info",
                method: 'post',
                data: data,
                success: function (json) {
                    console.log(json);
                    console.log(data);
                    if (!json) {
                        var message = "Something failed! Please try again";
                        $.alert(message, {
                            autoClose: true,
                            closeTime: 3000,
                            withTime: false,
                            type: 'danger',
                            position: ['top-right', [0, 0]],
                            speed: 'normal',
                            isOnly: true,
                            onClose: function () {

                            }
                        });
                    } else {
                        $.alert("Update user info successfully!", {
                            autoClose: true,
                            closeTime: 3000,
                            withTime: false,
                            type: 'success',
                            position: ['top-right', [0, 0]],
                            speed: 'normal',
                            isOnly: true,
                            onClose: function () {
                                window.location.href = '/userinfo'
                            }
                        });
                    }
                }
            })
        }
    });
});