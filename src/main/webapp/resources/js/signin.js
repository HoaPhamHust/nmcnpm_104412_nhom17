$('document').ready(function()
{

    $("#register-form").submit(function (e) {
        e.preventDefault();
    }).validate({

        errorPlacement : function(error, element) {
            $(element).closest('.form-group').find('.help-block').html(error.html());
        },
        highlight : function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.help-block').html('');
        },

        submitHandler: function(form){

            var data = $(form).serialize();

            $.ajax({
                url: "/login",
                method: 'post',
                data: data,
                success: function (json) {
                    if (json.status == "false") {
                        var message = json.message;
                        $.alert(message, {
                            autoClose: true,
                            closeTime: 3000,
                            withTime: false,
                            type: 'danger',
                            position: ['top-right', [0, 0]],
                            speed: 'normal',
                            isOnly: true,
                            onClose: function () {

                            }
                        });
                    } else {
                        var redicrect = json.redirect;
                        $.alert("Login successfully!", {
                            autoClose: true,
                            closeTime: 3000,
                            withTime: false,
                            type: 'success',
                            position: ['top-right', [0, 0]],
                            speed: 'normal',
                            isOnly: true,
                            onClose: function () {
                                window.location.href = redicrect
                            }
                        });
                    }
                }
            })

        }
    });
});