<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.nhom17.model.dto.Phim"%>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="com.nhom17.model.dto.TheLoai" %>
<%@ page contentType="text/html; charset=UTF-8"
	%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="${pageContext.request.contextPath}/resources/css/showYtVideo.css"
	rel="stylesheet" />
</head>
<body>
	<%
		Phim movie = (Phim) request.getAttribute("movie");
		String date = movie.getNgayBatDau().format(DateTimeFormatter.ofPattern("MMMM dd, yyyy"));
	%>
	<h2 class="page-heading"><%=movie.getTenPhim()%></h2>
	<div class="movie__info">
		<div class="col-sm-4 col-md-3 movie-mobile">
			<div class="movie__images">
				<span class="movie__rating"><%=movie.getImdbRank()%></span> <img alt=''
					src="<%=movie.getPosterURL()%>" style="width: 100%;">
			</div>
			<!-- <div class="movie__rate">Your vote: <div id='score' class="score"></div></div> -->
		</div>
		<div class="col-sm-8 col-md-9">
			<p class="movie__time"><%=movie.getThoiLuongPhim()%></p>

			<p class="movie__option">
				<strong>Country: </strong><a href="#"><%=movie.getQuocGia()%></a>
			</p>

			<p class="movie__option">
				<strong>Year: </strong><a href="#"><%=movie.getNgayBatDau().getYear()%></a>
			</p>
			<p class="movie__option">
				<%
					/* 
					1- > Action
					2-> Comedy
					3-> Animation
					4-> Action
					5-> Drama
					6-> Romantic
					7-> Adventure
					8-> Sports
					9-> War */
					String gerne = movie.getTheLoaiList().stream().map(TheLoai::getTenTheLoai).collect(Collectors.joining(", "));

					if (gerne == null || gerne.isEmpty()) {
					    gerne = "Action";
					}

				%>
				<strong>Category: </strong><a><%=gerne%></a>
			</p>
			<p class="movie__option">
				<strong>Release date: </strong>
				<!-- November 1, 2013 --><%=date%>
			</p>
			<p class="movie__option">
				<strong>Director: </strong><a><%=movie.getDaodien()%></a>
			</p>
			<p class="movie__option">
				<strong>Actors: </strong><a><%=movie.getDienVien()%></a>
			</p>
			<p class="movie__option">
				<strong>Age restriction: </strong><a><%=movie.getNhanPhim()%></a>
			</p>
		</div>
	</div>
</body>
</html>