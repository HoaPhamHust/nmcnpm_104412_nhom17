<%@page import="java.util.List"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.time.ZoneId" %>
<%@ page import="com.nhom17.model.dto.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="${pageContext.request.contextPath}/resources/css/showYtVideo.css"
	rel="stylesheet" />
</head>
<body>
<%
	MovieShowTimeSchedule movieShowTimeSchedule = (MovieShowTimeSchedule) request.getAttribute("movie");
	Phim movie = movieShowTimeSchedule.getMovie();
	String url = request.getContextPath() + "/movie?id=" + movie.getMaPhim();
%>
	<!-- Movie preview item -->
	<div class="movie movie--preview release">
		<div class="col-sm-5 col-md-3">
			<div class="movie__images">
				<img href='<%=url%>' alt=''
					src="<%= movie.getPosterURL()%>" style="width: 100%;">
			</div>
			<!-- <div class="movie__feature">
				<a href="#" class="movie__feature-item movie__feature--comment">123</a>
				<a href="#" class="movie__feature-item movie__feature--video">7</a>
				<a href="#" class="movie__feature-item movie__feature--photo">352</a>
			</div> -->
		</div>
		<div class="col-sm-7 col-md-9">
			<a href='<%=url%>' class="movie__title link--huge"><%=movie.getTenPhim()%></a>

			<p class="movie__time"><%=movie.getThoiLuongPhim()%>
				min
			</p>

			<p class="movie__option">
				<strong>Country: </strong><a href="#">USA</a>
			</p>
			<p class="movie__option">
				<%
					/* 
					1- > Action
					2-> Comedy
					3-> Animation
					4-> Action
					5-> Drama
					6-> Romantic
					7-> Adventure
					8-> Sports
					9-> War */
					String gerne = movie.getTheLoaiList().stream().map(TheLoai::getTenTheLoai).collect(Collectors.joining(", "));

					if (gerne == null || gerne.isEmpty()) {
						gerne = "Action";
					}
				%>
				<strong>Category: </strong><a href="#"><%=gerne%></a>
			</p>
			<p class="movie__option">
				<strong>Release date: </strong>
				<%
					SimpleDateFormat ft = new SimpleDateFormat(" dd MMMM, yyyy");
					Date date1 = Date.from(movie.getNgayBatDau().atStartOfDay(ZoneId.systemDefault()).toInstant());
					String date = ft.format(date1);
//					String date = movie.getNgayBatDau().toString();
				%>
				<!-- November 1, 2013 --><%=date%>
			</p>
			<p class="movie__option">
				<strong>Director: </strong><a><%=movie.getDaodien()%></a>
			</p>
			<p class="movie__option">
				<strong>Actors: </strong><a><%=movie.getDienVien()%></a>
			</p>
			<p class="movie__option">
				<strong>Maturity: </strong><a><%=movie.getNhanPhim()%></a>
			</p>

			<div class="movie__btns">
				<%-- <%
					if (movie.isNowShowingFlag()) {
				%>
				<a href="#" class="btn btn-md btn--warning">book a ticket for
					this movie</a>
				<%
					} else {
				%>
				<a href="#" class="btn btn-md btn--warning disabled">Coming Soon</a>
				<%
					}
				%> --%>
				<%--<a href="javascript:void(0);" class="watchlist"--%>
					<%--onclick="showTrailer('<%=movie.()%>')">Watch Trailer</a>--%>
			</div>
			<%--<div class="preview-footer">--%>
				<%--<div class="movie__rate" style="padding-bottom: 50px;">--%>
					<%--<!-- <div class="score"></div>--%>
					<%--<span class="movie__rate-number">170 votes</span> -->--%>
					<%--<span class="movie__rating">5.0</span>--%>
				<%--</div>--%>
				<%--&lt;%&ndash;&lt;%&ndash;%>--%>
					<%--&lt;%&ndash;Map<DangPhim,List<XuatChieu>> map = movieShowTimeSchedule.getShowTimes();&ndash;%&gt;--%>
					<%--&lt;%&ndash;int i = 0;&ndash;%&gt;--%>
					<%--&lt;%&ndash;for (Map.Entry<DangPhim, List<XuatChieu>> entry : map.entrySet())&ndash;%&gt;--%>
					<%--&lt;%&ndash;{&ndash;%&gt;--%>
					    <%--&lt;%&ndash;List<XuatChieu> list = entry.getValue();&ndash;%&gt;--%>
						<%--&lt;%&ndash;if (!list.isEmpty()) {&ndash;%&gt;--%>
						    <%--&lt;%&ndash;i++;&ndash;%&gt;--%>
				<%--&lt;%&ndash;%>&ndash;%&gt;--%>
					<%--&lt;%&ndash;<a href="#" class="movie__show-btn movie__show-btn<%=i%>"><%=entry.getKey().getTenDangPhim()%></a>&ndash;%&gt;--%>
				<%--&lt;%&ndash;&lt;%&ndash;%>--%>
						<%--&lt;%&ndash;}&ndash;%&gt;--%>
					<%--&lt;%&ndash;}&ndash;%&gt;--%>
				<%--&lt;%&ndash;%>&ndash;%&gt;--%>
			<%--</div>--%>
		</div>
		<div class="clearfix"></div>
		<%--<jsp:include page="/WEB-INF/templates/web/movie_time.jsp" />--%>
	</div>
</body>
</html>