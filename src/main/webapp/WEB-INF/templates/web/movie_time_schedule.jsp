<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.nhom17.model.dto.XuatChieu"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%!Date getZeroTimeDate(Date fecha, boolean flag) {
	Date res = fecha;
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(fecha);
	if (flag) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		res = calendar.getTime();
	} else {
		calendar.set(Calendar.DATE, 0);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.YEAR, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}
	return res;
}%>

<%
	String format = (String) request.getAttribute("format");
	List<XuatChieu> showTimeSchedule = (ArrayList<XuatChieu>) request.getAttribute("schedule");
	Date todayDate = new Date();
	Date showDate = (Date) request.getAttribute("showdate");
	String datePickerDate = new SimpleDateFormat("MM/dd/yyyy").format(showDate);
	String id = (String) request.getAttribute("maPhim");
	String bookingURL = request.getContextPath() + "/" + "booking_step2?" + "movie_id=" + id
			+ "&" + "date=" + datePickerDate;
	int isToday = 0;
	if (showDate.compareTo(getZeroTimeDate((Date) todayDate.clone(), true)) > 1) {
		isToday = 1;
	} else if (showDate.compareTo(getZeroTimeDate((Date) todayDate.clone(), true)) < 0) {
		isToday = -1;
	}
%>
<body>
<h3 class="page-heading" style="font-size: 13px;">
<%=format%>
</h3>
<div class="choose-container">

	<div class="clearfix"></div>

	<div class="time-select">
		<div class="time-select__group">
			<ul class="col-sm-8 items-wrap">
				<%
					for (XuatChieu showTime : showTimeSchedule) {
						System.out.println(showTime.getThoiGianChieu());
						String clsName = "";
						String showURL = null;
						switch (isToday) {
							case -1:
								System.out.print(-1);
								clsName = "disabled";
								break;
							case 1:
								clsName = "";
								showURL = bookingURL + "&time=" + showTime.getThoiGianChieu() + "&showtime=" + showTime.getMaXuatChieu().replaceAll(" ", "_");
								break;
							default:
								Date dupShowTime = new SimpleDateFormat("MM/dd/yyyy hh:mm")
										.parse(datePickerDate + " " + showTime.getThoiGianChieu());
								System.out.println(dupShowTime);
								System.out.println(todayDate);
								System.out.println(dupShowTime.compareTo(todayDate));
								if (dupShowTime.compareTo(todayDate) <= 0) {
									clsName = "disable";
								} else {
									clsName = "";
									showURL = bookingURL + "&time=" + showTime.getThoiGianChieu() + "&showtime=" + showTime.getMaXuatChieu().replaceAll(" ", "_");
								}
								break;
						}
				%>
				<li class="time-select__item <%=clsName%>"
					data-time='<%=showTime.getThoiGianChieu().substring(0, 5)%>'
					onclick="bookSeat('<%=showURL%>')"><%=showTime.getThoiGianChieu().substring(0, 5)%></li>
				<%
					}
				%>
			</ul>
		</div>
	</div>
</div>
</body>
</html>