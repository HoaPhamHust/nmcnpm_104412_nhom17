<%@ page language="java" contentType="text/html; charset=UTF-8"
	%>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
	<!-- JavaScript-->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!-- Migrate -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-migrate-1.2.1.min.js"></script>
	<!-- jQuery UI -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
	<!-- Bootstrap 3-->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

	<!-- Mobile menu -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.mobile.menu.js"></script>
	<!-- Select -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.selectbox-0.2.min.js"></script>

	<!-- Stars rate -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.raty.js"></script>
	<!-- Form element -->
	<script
		src="${pageContext.request.contextPath}/resources/js/form-element.js"></script>
	<!-- Form validation -->
	<script src="${pageContext.request.contextPath}/resources/js/form.js"></script>
	<!-- Magnific-popup -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.magnific-popup.min.js"></script>
	<!-- Custom -->
	
	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/jQuery.countdownTimer.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resources/css/jQuery.countdownTimer.css" />
	
</body>
</html>