/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nhom17.util;

import javax.servlet.http.HttpServletRequest;

import static java.lang.Integer.min;

/**
 *
 * @author letha
 */
public class Utils {
    public static String briefText(String text,int maxLen) {
        StringBuilder tmp = new StringBuilder();
        int textLen = text.length();
        
        for (int i = 0; i < min(maxLen,textLen); i++) 
            tmp.append(text.charAt(i));
        
        if (maxLen < textLen) 
            tmp.append("...");
        
        return tmp.toString();
    }

    public static String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURI());
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }
}
