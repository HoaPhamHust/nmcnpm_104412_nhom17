package com.nhom17.controllers;

import com.nhom17.model.dto.Phim;
import com.nhom17.model.reposity.impl.PhimDao;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;

@WebServlet(name = "InsertMovieControllerServlet", urlPatterns = {"/InsertMovieControllerServlet"})
public class InsertMovieControllerServlet extends BaseServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        LocalTime thoiluongPhim = LocalTime.parse(request.getParameter("thoiluongPhim"));
        LocalDate ngayBatDau = LocalDate.parse(request.getParameter("ngayBatDau"));
        LocalDate ngayKetThuc = LocalDate.parse(request.getParameter("ngayKetThuc"));
        String quocGia = request.getParameter("quocGia");
        String ghiChu = request.getParameter("ghiChu");
        String daoDien = request.getParameter("daoDien");
        String dienVien = request.getParameter("dienVien");
        String[] theLoaiList = request.getParameterValues("theLoaiList");
        String imdbURL = request.getParameter("imdbURL");
        Float IMDBRank = Float.valueOf(request.getParameter("IMDBRank"));
        String tenPhim = request.getParameter("tenPhim");
        String nhanPhim = request.getParameter("nhanPhim");
        String motaPhim = request.getParameter("motaPhim");
        String posterURL = request.getParameter("posterURL");
        String poster2URL = request.getParameter("poster2URL");

//
        Phim phim = new Phim();
        phim.setDaodien(daoDien);
        phim.setDienVien(dienVien);
        phim.setTenPhim(tenPhim);
        phim.setThoiLuongPhim(thoiluongPhim);
        phim.setQuocGia(quocGia);
        phim.setGhiChu(ghiChu);
        phim.setImdbRank(IMDBRank);
        phim.setImdbURL(imdbURL);
        phim.setNhanPhim(nhanPhim);
        phim.setMotaPhim(motaPhim);
        phim.setPoster2URL(poster2URL);
        phim.setPosterURL(posterURL);
        phim.setNgayKetThuc(ngayKetThuc);
        phim.setNgayBatDau(ngayBatDau);
        PhimDao phimDao = PhimDao.createPhimReposity();


        int id = phimDao.insertMovie(phim);
        phim = phimDao.getMovie(id);

        for (String theloai: theLoaiList) {
            phimDao.insertTheLoaiPhim(phim.getMaPhim(), theloai);
        }
        
        List<Phim> movies = PhimDao.createPhimReposity().getAllMovies();
        request.setAttribute("ListMovie", movies);
        RequestDispatcher rd = request.getRequestDispatcher("pages/web/Admin/Home.jsp");
	rd.forward(request, response);
            
    }
}
