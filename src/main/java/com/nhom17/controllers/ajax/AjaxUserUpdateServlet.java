package com.nhom17.controllers.ajax;

import com.nhom17.controllers.BaseServlet;
import com.nhom17.model.dto.ThanhVien;
import com.nhom17.model.reposity.impl.ThanhVienDAO;
import com.nhom17.util.AppUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@WebServlet("/ajax/info")
public class AjaxUserUpdateServlet extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println(request.getParameter("birthday"));
        ThanhVien loginedUser = AppUtils.getLoginedUser(request.getSession());

        String name = request.getParameter("name");
        LocalDate birthday = LocalDate.parse(request.getParameter("birthday"), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        int gender = Integer.valueOf(request.getParameter("gender"));

        ThanhVien thanhVien = new ThanhVien();
        thanhVien.setTenDangNhap(loginedUser.getTenDangNhap());
        thanhVien.setHoTen(name);
        thanhVien.setNgaySinh(birthday);
        thanhVien.setEmail(email);
        thanhVien.setSoDienThoai(phone);
        thanhVien.setGioiTinh(gender);
        thanhVien.setDiaChi(address);

        ThanhVienDAO thanhVienDAO = ThanhVienDAO.createThanhVienReposity();
        int update = thanhVienDAO.update(thanhVien);

        if (update > 0) {
            thanhVien = thanhVienDAO.getByUserName(loginedUser.getTenDangNhap());
            AppUtils.storeLoginedUser(request.getSession(false), thanhVien);
        }

        response.setContentType("application/json");
        String output = update > 0 ? "true" : "false";
        PrintWriter writer = response.getWriter();
        writer.write(output);
        writer.close();
    }
}
