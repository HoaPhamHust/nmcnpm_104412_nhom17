/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nhom17.controllers;

import com.nhom17.model.dto.ThongKe;
import com.nhom17.model.reposity.impl.ThongKeDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author letha
 */
@WebServlet(name = "ThongKeByDate", urlPatterns = {"/ThongKeByDate"})
public class ThongKeByDate extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ThongKeByDate</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ThongKeByDate at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String button = request.getParameter("button");
        if("button1".equals(button)){
            String ngayChieu = request.getParameter("ngayChieu");
            request.setAttribute("ngayChieu", ngayChieu);
            ThongKeDao repository = ThongKeDao.createThongKeReposity();
            List<ThongKe> thongKeList = repository.getThongKeByDate(ngayChieu);
            request.setAttribute("ListThongKe", thongKeList);
            RequestDispatcher rd = request.getRequestDispatcher("pages/web/Admin/ThongKeByDate.jsp");
            rd.forward(request, response);
        }
        if("button2".equals(button)){
            String ngayChieu = request.getParameter("ngayChieu");
            request.setAttribute("ngayChieu", ngayChieu);
            ThongKeDao repository = ThongKeDao.createThongKeReposity();
            List<ThongKe> thongKeList = repository.getThongKeByMonth(ngayChieu);
            request.setAttribute("ListThongKe", thongKeList);
            RequestDispatcher rd = request.getRequestDispatcher("pages/web/Admin/ThongKeByMonth.jsp");
            rd.forward(request, response);
        }
        if("button3".equals(button)){
            String ngayChieu = request.getParameter("ngayChieu");
            request.setAttribute("ngayChieu", ngayChieu);
            ThongKeDao repository = ThongKeDao.createThongKeReposity();
            List<ThongKe> thongKeList = repository.getThongKeByYear(ngayChieu);
            request.setAttribute("ListThongKe", thongKeList);
            RequestDispatcher rd = request.getRequestDispatcher("pages/web/Admin/ThongKeByYear.jsp");
            rd.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
