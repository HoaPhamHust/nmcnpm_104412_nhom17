/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nhom17.controllers;

import com.nhom17.model.dto.Phim;
import com.nhom17.model.reposity.impl.PhimDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author letha
 */
@WebServlet(name = "UpdateMovieControllerServlet", urlPatterns = {"/UpdateMovieControllerServlet"})
public class UpdateMovieControllerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateMovieControllerServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateMovieControllerServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String maPhim = request.getParameter("maPhim");
        LocalTime thoiluongPhim = LocalTime.parse(request.getParameter("thoiluongPhim"));
        LocalDate ngayBatDau = LocalDate.parse(request.getParameter("ngayBatDau"));
        LocalDate ngayKetThuc = LocalDate.parse(request.getParameter("ngayKetThuc"));
        String quocGia = request.getParameter("quocGia");
        String ghiChu = request.getParameter("ghiChu");
        String daoDien = request.getParameter("daoDien");
        String dienVien = request.getParameter("dienVien");
        String[] theLoaiList = request.getParameterValues("theLoaiList");
        String imdbURL = request.getParameter("imdbURL");
        Float IMDBRank = Float.valueOf(request.getParameter("IMDBRank"));
        String tenPhim = request.getParameter("tenPhim");
        String nhanPhim = request.getParameter("nhanPhim");
        String motaPhim = request.getParameter("motaPhim");
        String posterURL = request.getParameter("posterURL");
        String poster2URL = request.getParameter("poster2URL");

//
        Phim phim = new Phim();
        phim.setDaodien(daoDien);
        phim.setDienVien(dienVien);
        phim.setTenPhim(tenPhim);
        phim.setThoiLuongPhim(thoiluongPhim);
        phim.setQuocGia(quocGia);
        phim.setGhiChu(ghiChu);
        phim.setImdbRank(IMDBRank);
        phim.setImdbURL(imdbURL);
        phim.setNhanPhim(nhanPhim);
        phim.setMotaPhim(motaPhim);
        phim.setPoster2URL(poster2URL);
        phim.setPosterURL(posterURL);
        phim.setNgayKetThuc(ngayKetThuc);
        phim.setNgayBatDau(ngayBatDau);
        phim.setMaPhim(maPhim);
        PhimDao phimDao = PhimDao.createPhimReposity();
        
        
        
        phimDao.updateMovie(phim);
        
        List<Phim> movies = PhimDao.createPhimReposity().getAllMovies();
        request.setAttribute("ListMovie", movies);
        RequestDispatcher rd = request.getRequestDispatcher("pages/web/Admin/Home.jsp");
	rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
