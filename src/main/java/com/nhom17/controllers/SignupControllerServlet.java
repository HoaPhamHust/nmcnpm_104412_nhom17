package com.nhom17.controllers;

import com.nhom17.model.dto.ThanhVien;
import com.nhom17.model.reposity.impl.PhimDao;
import com.nhom17.model.reposity.impl.ThanhVienDAO;
import com.nhom17.util.AppUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;

//@WebServlet("/ajax/user")
public class SignupControllerServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String redirectID = request.getParameter("redirectID");

        ThanhVien newUser = new ThanhVien();
        newUser.setHoTen(fullname);
        newUser.setTenDangNhap(username);
        newUser.setEmail(email);
        newUser.setMatKhau(password);
        newUser.setLoaiTV("USER");
        newUser.setNgaySinh(LocalDate.of(1970,1,1));
        newUser.setGioiTinh(0);

        ThanhVienDAO reposity = ThanhVienDAO.createThanhVienReposity();
        int i = reposity.add(newUser);
        JSONObject json = new JSONObject();

        if(i < 1) {
            try {
                json.put("status", "false");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                json.put("status", "true");
                json.put("redirectId", redirectID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String output = json.toString();
        PrintWriter writer = response.getWriter();
        writer.write(output);
        writer.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (AppUtils.getLoginedUser(request.getSession(false)) != null) {
            response.sendRedirect("/home");
        }
        RequestDispatcher dispatcher //
                = request.getRequestDispatcher("/signup_page");
        dispatcher.forward(request, response);
    }
}
