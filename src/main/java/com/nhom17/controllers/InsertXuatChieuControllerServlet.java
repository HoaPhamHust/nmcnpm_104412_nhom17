/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nhom17.controllers;

import com.nhom17.model.dto.XuatChieu;
import com.nhom17.model.reposity.impl.XuatChieuDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author letha
 */
@WebServlet(name = "InsertXuatChieuControllerServlet", urlPatterns = {"/InsertXuatChieuControllerServlet"})
public class InsertXuatChieuControllerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertXuatChieuControllerServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertXuatChieuControllerServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        LocalDate ngayChieu = LocalDate.parse(request.getParameter("ngayChieu"));
        String thoiGianChieu = request.getParameter("thoiGianChieu");
        String maPhim = request.getParameter("maPhim");
        String maPhong = request.getParameter("maPhong");
        String maDangPhim = request.getParameter("maDangPhim");
        
        XuatChieu xuatChieu = new XuatChieu();
        xuatChieu.setMaDangPhim(maDangPhim);
        xuatChieu.setMaPhim(maPhim);
        xuatChieu.setMaPhong(maPhong);
        xuatChieu.setNgayChieu(ngayChieu);
        xuatChieu.setThoiGianChieu(thoiGianChieu);
        XuatChieuDao xuatchieuDao = XuatChieuDao.createXuatChieuReposity();
        
        xuatchieuDao.insertXuatChieu(xuatChieu);
        
        List<XuatChieu> ListXuatChieu = XuatChieuDao.createXuatChieuReposity().getAllXuatChieuForNowShowingMovie();
        request.setAttribute("ListXuatChieu", ListXuatChieu);
        
        RequestDispatcher rd = request.getRequestDispatcher("pages/web/Admin/XuatChieu.jsp");
	rd.forward(request, response);
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
