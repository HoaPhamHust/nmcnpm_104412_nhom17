package com.nhom17.model.reposity.impl;

import com.nhom17.model.dto.Phim;
import com.nhom17.model.dto.XuatChieu;
import com.nhom17.util.JdbcTemplate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class XuatChieuDao extends CommonDao<XuatChieu> {

    public final static String ID = "id";
    public final static String THOI_GIAN_CHIEU = "ThoiGianChieu";
    public final static String NGAY_CHIEU = "NgayChieu";
    public final static String MA_PHIM = "MaPhim";
    public final static String MA_DANG_PHIM = "MaDangPhim";
    public final static String MA_XUAT_CHIEU = "MaXuatChieu";
    public final static String MA_PHONG = "MaPhong";

    private XuatChieuDao() {
    }

    public static XuatChieuDao createXuatChieuReposity() {
        return new XuatChieuDao();
    }

    @Override
    public XuatChieu getOne(final String id) {
        return JdbcTemplate.singleQuery("SELECT * FROM [dbo].[XuatChieu] WHERE MaXuatChieu = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, id);
            }
        }, createHandler());
    }

    public List<XuatChieu> getByMovie(final Phim phim){
        final Date date = new Date();
        List<XuatChieu> xuatChieuList = JdbcTemplate.query("SELECT * FROM [dbo].[XuatChieu] WHERE MaPhim = ? AND NgayChieu >= ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, phim.getMaPhim());
                pstmt.setDate(2, new java.sql.Date(date.getTime()));
            }
        }, createHandler());

        return xuatChieuList;
    }

    public List<XuatChieu> getAllXuatChieu(){
        List<XuatChieu> xuatChieuList =
                JdbcTemplate.query("SELECT * FROM [dbo].[XuatChieu] ORDER BY NgayChieu", createHandler());

        return xuatChieuList;
    }

    public List<XuatChieu> getByMovie(final Phim phim, final Date date){
        List<XuatChieu> xuatChieuList = JdbcTemplate.query("SELECT * FROM [dbo].[XuatChieu] WHERE MaPhim = ? AND NgayChieu = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, phim.getMaPhim());
                java.sql.Date date1 = new java.sql.Date(date.getTime());
                pstmt.setDate(2, date1);
            }
        }, createHandler());

        return xuatChieuList;
    }

    public int insertXuatChieu(final XuatChieu xuatChieu) {
        return JdbcTemplate.update("INSERT INTO [dbo].[XuatChieu](MaPhim,MaPhong,ThoiGianChieu,NgayChieu," +
                        "MaDangPhim) VALUES (?,?,?,?,?)",
                new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1,xuatChieu.getMaPhim());
                        pstmt.setString(2,xuatChieu.getMaPhong());
                        pstmt.setString(3,xuatChieu.getThoiGianChieu());
                        pstmt.setObject(4,xuatChieu.getNgayChieu());
                        pstmt.setString(5,xuatChieu.getMaDangPhim());
                    }
                });
    }

    public List<XuatChieu> getAllXuatChieuForNowShowingMovie() {
        List<XuatChieu> xuatChieuList = JdbcTemplate.query("SELECT XuatChieu.*,TenPhim FROM [dbo].[XuatChieu]," +
                "[dbo].[DanhSachPhimDangChieu] WHERE XuatChieu.MaPhim = DanhSachPhimDangChieu.MaPhim ", createHandler());
        return xuatChieuList;
    }

    public int deleteXuatChieu(final String id) {
        return JdbcTemplate.update("DELETE FROM [dbo].[XuatChieu] WHERE MaXuatChieu = ?",
                new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1,id);
                    }
                });
    }

    public int updateXuatChieu(final XuatChieu xuatChieu) {
        return JdbcTemplate.update("UPDATE [dbo].[XuatChieu] SET " +
                        "MaPhim = ?,MaPhong = ?,ThoiGianChieu = ?,NgayChieu = ?,MaDangPhim = ? " +
                        "WHERE MaXuatChieu = ?",
                new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1,xuatChieu.getMaPhim());
                        pstmt.setString(2,xuatChieu.getMaPhong());
                        pstmt.setString(3,xuatChieu.getThoiGianChieu());
                        pstmt.setObject(4,xuatChieu.getNgayChieu());
                        pstmt.setString(5,xuatChieu.getMaDangPhim());
                        pstmt.setString(6,xuatChieu.getMaXuatChieu());

                    }
                });
    }

    private JdbcTemplate.RowCallBackHandler<XuatChieu> createHandler() {
        return new JdbcTemplate.RowCallBackHandler<XuatChieu>() {
            @Override
            public XuatChieu processRow(ResultSet rs) throws SQLException {
                XuatChieu xuatChieu = new XuatChieu();
                xuatChieu.setId(rs.getInt(ID));
                xuatChieu.setMaDangPhim(rs.getString(MA_DANG_PHIM));
                xuatChieu.setMaPhim(rs.getString(MA_PHIM));
                xuatChieu.setMaPhong(rs.getString(MA_PHONG));
                xuatChieu.setMaXuatChieu(rs.getString(MA_XUAT_CHIEU));
                String time = String.valueOf(rs.getTime(THOI_GIAN_CHIEU));
                time = time.substring(0,5);
                xuatChieu.setThoiGianChieu(time);
                try {
                    String tenPhim = rs.getString("TenPhim");
                    xuatChieu.setTenPhim(tenPhim);
                } catch (SQLException sqlEx) {

                }
                xuatChieu.setNgayChieu(rs.getObject(NGAY_CHIEU, LocalDate.class));
                return xuatChieu;
            }
        };
    }
}
