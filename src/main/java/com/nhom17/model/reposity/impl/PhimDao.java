package com.nhom17.model.reposity.impl;

import com.nhom17.model.dto.Phim;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import com.nhom17.model.dto.TheLoai;
import com.nhom17.util.JdbcTemplate;

public class PhimDao extends CommonDao<Phim> {

    public static String ID = "ID";
    public static String MA_PHIM = "MaPhim";
    public static String TEN_PHIM = "TenPhim";
    public static String QUOC_GIA = "QuocGia";
    public static String THOI_LUONG_PHIM = "ThoiLuongPhim";
    public static String NGAY_BAT_DAU = "NgayBatDau";
    public static String NGAY_KET_THUC = "NgayKetThuc";
    public static String POSTER_1_URL = "PosterURL1";
    public static String MO_TA_PHIM = "MotaPhim";
    public static String GHI_CHU = "GhiChu";
    public static String NHAN_PHIM = "NhanPhim";
    public static String IMDB_RANK = "IMDBrank";
    public static String IMDB_URL = "IMDBURL";
    public static String POSTER_2_URL = "PosterURL2";
    public static String DAO_DIEN = "DaoDien";
    public static String DIEN_VIEN = "DienVien";

    private PhimDao() {
    }

    public static PhimDao createPhimReposity() {
        return new PhimDao();
    }

    public int deleteMovie(final String id) {
        return JdbcTemplate.update("DELETE FROM [dbo].[Phim] WHERE MaPhim = ?",
                new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1,id);
                    }
                });
    }
    public ArrayList<String> getTenPhimInXuatChieu() {
        ArrayList<String> listTenPhim = new ArrayList<>();
        List<Phim> phimList = JdbcTemplate.query("SELECT DaoDien,DienVien,GhiChu,IMDBURL," +
                "MotaPhim,NhanPhim,PosterURL2,PosterURL1,QuocGia,TenPhim,IMDBrank," +
                "NgayBatDau,NgayKetThuc,ThoiLuongPhim " +
                "FROM [dbo].[XuatChieu] INNER JOIN [dbo].[Phim] ON XuatChieu.MaPhim = Phim.MaPhim" +
                "ORDER BY ThoiGianChieu",createHandler());
        for (Phim phim: phimList) {
            listTenPhim.add(phim.getTenPhim());
        }
        return listTenPhim;
    }
    public List<Phim> getAllMovies() {
        return JdbcTemplate.query("SELECT * FROM [dbo].[Phim]",createHandler());
    }

    public List<Phim> getNowShowingMovies() {
        return JdbcTemplate.query("SELECT * FROM [dbo].[DanhSachPhimDangChieu]",createHandler());
    }

    public List<Phim> getUpComingMovies() {
        return JdbcTemplate.query("SELECT * FROM [dbo].[DanhSachPhimSapChieu]",createHandler());
    }
    
    public int updateMovie(final Phim phim) {
        return JdbcTemplate.update("UPDATE [dbo].[Phim] SET " +
                        "DaoDien = ?,DienVien = ?,GhiChu = ?,IMDBURL = ?," +
                        "MotaPhim = ?,NhanPhim = ?,PosterURL2 = ?,PosterURL1 = ?," +
                        "QuocGia = ?,TenPhim = ?,IMDBrank = ?," +
                        "NgayBatDau = ?,NgayKetThuc = ?,ThoiLuongPhim = ? " +
                        "WHERE MaPhim = ?",
                new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1,phim.getDaodien());
                        pstmt.setString(2,phim.getDienVien());
                        pstmt.setString(3,phim.getGhiChu());
                        pstmt.setString(4,phim.getImdbURL());
                        pstmt.setString(5,phim.getMotaPhim());
                        pstmt.setString(6,phim.getNhanPhim());
                        pstmt.setString(7,phim.getPoster2URL());
                        pstmt.setString(8,phim.getPosterURL());
                        pstmt.setString(9,phim.getQuocGia());
                        pstmt.setString(10,phim.getTenPhim());
                        pstmt.setFloat(11,phim.getImdbRank());
//                        pstmt.setDate(12, java.sql.Date.valueOf(phim.getNgayBatDau()));
                        pstmt.setObject(12,phim.getNgayBatDau());
                        pstmt.setObject(13,phim.getNgayKetThuc());
                        pstmt.setObject(14,phim.getThoiLuongPhim());
                        pstmt.setObject(15,phim.getMaPhim());
                        
                    }
                });
    }
    public int insertMovie(final Phim phim) {
        return JdbcTemplate.update("INSERT INTO [dbo].[Phim](DaoDien,DienVien,GhiChu,IMDBURL," +
                        "MotaPhim,NhanPhim,PosterURL2,PosterURL1,QuocGia,TenPhim,IMDBrank," +
                        "NgayBatDau,NgayKetThuc,ThoiLuongPhim) " +
                        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1,phim.getDaodien());
                        pstmt.setString(2,phim.getDienVien());
                        pstmt.setString(3,phim.getGhiChu());
                        pstmt.setString(4,phim.getImdbURL());
                        pstmt.setString(5,phim.getMotaPhim());
                        pstmt.setString(6,phim.getNhanPhim());
                        pstmt.setString(7,phim.getPoster2URL());
                        pstmt.setString(8,phim.getPosterURL());
                        pstmt.setString(9,phim.getQuocGia());
                        pstmt.setString(10,phim.getTenPhim());
                        pstmt.setFloat(11,phim.getImdbRank());
//                        pstmt.setDate(12, java.sql.Date.valueOf(phim.getNgayBatDau()));
                        pstmt.setObject(12,phim.getNgayBatDau());
                        pstmt.setObject(13,phim.getNgayKetThuc());
                        pstmt.setObject(14,phim.getThoiLuongPhim());
                    }
                });
    }

//    public ArrayList<String> getTenPhimInXuatChieu() {
//        ArrayList<String> listTenPhim = new ArrayList<>();
//        List<Phim> phimList = JdbcTemplate.query("SELECT DaoDien,DienVien,GhiChu,IMDBURL," +
//                "MotaPhim,NhanPhim,PosterURL2,PosterURL1,QuocGia,TenPhim,IMDBrank," +
//                "NgayBatDau,NgayKetThuc,ThoiLuongPhim,ThoiGianChieu " +
//                "FROM [dbo].[XuatChieu] INNER JOIN [dbo].[Phim] ON XuatChieu.MaPhim = Phim.MaPhim" +
//                "ORDER BY ThoiGianChieu",createHandler());
//        for (Phim phim: phimList) {
//            listTenPhim.add(phim.getTenPhim());
//        }
//        return listTenPhim;
//    }

    public int insertTheLoaiPhim(final String movieID, final String maTheLoai) {
        return JdbcTemplate.update("INSERT INTO [dbo].[TheLoaiPhim](MaTheLoai, MaPhim) " +
                "VALUES (?,?)", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1,maTheLoai);
                pstmt.setString(2,movieID);
            }
        });

    }

    public Phim getMovie(final String id) {
        return JdbcTemplate.singleQuery("SELECT * FROM [dbo].[Phim] WHERE MaPhim=?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1,id);
            }
        }, createHandler());
    }

    public Phim getMovie(final int id) {
        return JdbcTemplate.singleQuery("SELECT * FROM [dbo].[Phim] WHERE ID=?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setInt(1,id);
            }
        }, createHandler());
    }


    public Phim getMovieByName(final String name) {
        return JdbcTemplate.singleQuery("SELECT * FROM [dbo].[Phim] WHERE TenPhim = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1,name);
            }
        }, createHandler());
    }




    private JdbcTemplate.RowCallBackHandler<Phim> createHandler() {
        return new JdbcTemplate.RowCallBackHandler<Phim>() {
            @Override
            public Phim processRow(ResultSet rs) throws SQLException {
                Phim phim = new Phim();
                phim.setId(rs.getInt(ID));
                phim.setMaPhim(rs.getString(MA_PHIM));
                phim.setTenPhim(rs.getNString(TEN_PHIM));
                phim.setQuocGia(rs.getNString(QUOC_GIA));
//                phim.setThoiLuongPhim(rs.getTime(THOI_LUONG_PHIM).toLocalTime());
                phim.setThoiLuongPhim(rs.getObject(THOI_LUONG_PHIM, LocalTime.class));
                phim.setNgayBatDau(rs.getObject(NGAY_BAT_DAU,LocalDate.class));
                phim.setNgayKetThuc(rs.getObject(NGAY_KET_THUC, LocalDate.class));
                String poster = rs.getString(POSTER_1_URL);
                if (poster == null){
                    poster = "/resources/images/m1.jpg";
                }
                phim.setPosterURL(poster);
                phim.setMotaPhim(rs.getNString(MO_TA_PHIM));
                phim.setGhiChu(rs.getString(GHI_CHU));
                phim.setNhanPhim(rs.getString(NHAN_PHIM));
                phim.setImdbRank(rs.getFloat(IMDB_RANK));
                phim.setImdbURL(rs.getString(IMDB_URL));
                phim.setDaodien(rs.getNString(DAO_DIEN));
                phim.setDienVien(rs.getNString(DIEN_VIEN));

                List<TheLoai> theLoaiList = TheLoaiDao.createTheLoaiReposity().getByMovie(phim);
                phim.setTheLoaiList(theLoaiList);

                return phim;
            }
        };
    }
}
