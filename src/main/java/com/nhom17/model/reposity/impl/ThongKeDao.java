package com.nhom17.model.reposity.impl;

import com.nhom17.model.dto.Phim;
import com.nhom17.model.dto.ThongKe;
import com.nhom17.model.dto.XuatChieu;
import com.nhom17.util.JdbcTemplate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class ThongKeDao extends CommonDao<ThongKe> {
    public static final String NGAY_CHIEU = "NgayChieu";
    public static final String SO_LUONG_GHE_CUNG_CAP = "SoLuongGheCungCap";
    public static final String TONG_THU = "TongThu";
    public static final String SO_LUONG_GHE_DA_BAN = "SoLuongGheDaBan";
    public static final String MA_PHIM = "MaPhim";
    public static final String SO_LUONG_XUAT_CHIEU = "SoLuongXuatChieu";
    public static final String ID = "ID";
    private String[] time;

    private ThongKeDao() {
    }

    public static ThongKeDao createThongKeReposity() {
        return new ThongKeDao();
    }

    public List<ThongKe> getAllThongKe() {
        List<ThongKe> thongKeList = JdbcTemplate.query("SELECT ThongKe.*,TenPhim FROM [dbo].[ThongKe]," +
                "[dbo].[Phim] WHERE ThongKe.MaPhim = Phim.MaPhim ", createHandler());
        return thongKeList;
    }

    public List<ThongKe> getThongKeByMovie(final String movieID){
        List<ThongKe> thongKeList = JdbcTemplate.query("SELECT ThongKe.*,TenPhim FROM [dbo].[ThongKe], " +
                "[dbo].[Phim] WHERE ThongKe.MaPhim = Phim.MaPhim AND ThongKe.MaPhim = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, movieID);
            }
        }, createHandler());
        return thongKeList;
    }

    public List<ThongKe> getThongKeByDate(final String ngayChieu){
        String[] time = ngayChieu.split("/");
//        String ngaychieu = time[2] + "-" + time[1] + "-" + time[0];
        List<ThongKe> thongKeList = JdbcTemplate.query("SELECT ThongKe.*,TenPhim FROM [dbo].[ThongKe], " +
                "[dbo].[Phim] WHERE ThongKe.MaPhim = Phim.MaPhim AND ThongKe.NgayChieu = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                String ngayChieu2 = time[2] + "-" + time[1] + "-" + time[0];
                pstmt.setString(1, ngayChieu2);
            }
        }, createHandler());
        return thongKeList;
    }

    public List<ThongKe> getThongKeByMonth(final String ngayChieu){
        String[] time = ngayChieu.split("/");
        List<ThongKe> thongKeList = JdbcTemplate.query("SELECT ThongKe.*,TenPhim FROM [dbo].[ThongKe], " +
                "[dbo].[Phim] WHERE ThongKe.MaPhim = Phim.MaPhim " +
                "AND MONTH(ThongKe.NgayChieu) = ? AND YEAR(ThongKe.NgayChieu) = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, time[0]);
                pstmt.setString(2, time[1]);
            }
        }, createHandler());
        return thongKeList;
    }

    public List<ThongKe> getThongKeByYear(final String ngayChieu){
        String[] time = ngayChieu.split("/");
        List<ThongKe> thongKeList = JdbcTemplate.query("SELECT ThongKe.*,TenPhim FROM [dbo].[ThongKe], " +
                "[dbo].[Phim] WHERE ThongKe.MaPhim = Phim.MaPhim " +
                "AND YEAR(ThongKe.NgayChieu) = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, ngayChieu);
            }
        }, createHandler());
        return thongKeList;
    }

    private JdbcTemplate.RowCallBackHandler<ThongKe> createHandler() {
        return new JdbcTemplate.RowCallBackHandler<ThongKe>() {
            @Override
            public ThongKe processRow(ResultSet rs) throws SQLException {
                ThongKe thongKe = new ThongKe();
                thongKe.setId(rs.getInt(ID));
                thongKe.setSoLuongGheCungCap(rs.getInt(SO_LUONG_GHE_CUNG_CAP));
                thongKe.setMaPhim(rs.getString(MA_PHIM));
                thongKe.setTongThu(rs.getLong(TONG_THU));
                thongKe.setSoLuongGheDaBan(rs.getInt(SO_LUONG_GHE_DA_BAN));
                thongKe.setSoLuongXuatChieu(rs.getInt(SO_LUONG_XUAT_CHIEU));
                try {
                    String tenPhim = rs.getString("TenPhim");
                    thongKe.setTenPhim(tenPhim);
                } catch (SQLException sqlEx) {

                }
                thongKe.setNgayChieu(rs.getObject(NGAY_CHIEU, LocalDate.class));
                return thongKe;
            }
        };
    }

}
