package com.nhom17.model.reposity.impl;


import com.nhom17.model.dto.ThanhVien;
import com.nhom17.util.JdbcTemplate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class ThanhVienDAO extends CommonDao<ThanhVien> {
    public static final String EMAIL = "Email";
    public static final String LOAI_THANH_VIEN = "LoaiTV";
    public static final String MAT_KHAU = "MatKhau";
    public static final String GIOI_TINH = "GioiTinh";
    public static final String SO_DIEN_THOAI = "SoDienThoai";
    public static final String TEN_DANG_NHAP = "TenDangNhap";
    public static final String NGAY_SINH = "NgaySinh";
    public static final String HO_TEN = "HoTen";
    public static final String DIA_CHI = "DiaChi";
    public static final String SO_DU = "SoDu";

    private ThanhVienDAO() {

    }

    private static ThanhVienDAO instance = new ThanhVienDAO();

    public static ThanhVienDAO getInstance() {
        return instance;
    }

    public void setInstance(ThanhVienDAO instance) {
        this.instance = instance;
    }

    public static ThanhVienDAO createThanhVienReposity() {
        return new ThanhVienDAO();
    }

    //Tim thanh vien theo ten dang nhap va mat khau
    public ThanhVien getByUserNamePass(final String username, final String pass) {
        return JdbcTemplate.singleQuery("SELECT * FROM [dbo].[ThanhVien] WHERE (TenDangNhap = ? OR Email = ?) AND MatKhau = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, username);
                pstmt.setString(2, username);
                pstmt.setString(3, pass);
            }
        }, callBackHandler());
    }

    //Tim thanh vien theo ten dang nhap
    public ThanhVien getByUserName(final String username) {
        return JdbcTemplate.singleQuery("SELECT * FROM [dbo].[ThanhVien] WHERE TenDangNhap = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, username);
            }
        }, callBackHandler());
    }

    //Tim thanh vien theo email
    public ThanhVien getByEmail(final String email) {
        return JdbcTemplate.singleQuery("SELECT * FROM [dbo].[ThanhVien] WHERE Email = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, email);
            }
        }, callBackHandler());
    }

    //Them thanh vien
    public int add(ThanhVien item) {
        return JdbcTemplate.update(
                "INSERT INTO ThanhVien (TenDangNhap, MatKhau, HoTen, NgaySinh, GioiTinh, DiaChi, SoDienThoai, Email, SoDu, LoaiTV) values " +
                        "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1, item.getTenDangNhap());
                        pstmt.setString(2, item.getMatKhau());
                        pstmt.setString(3, item.getHoTen());
                        pstmt.setObject(4, item.getNgaySinh());
                        pstmt.setInt(5, item.getGioiTinh());
                        pstmt.setString(6, item.getDiaChi());
                        pstmt.setString(7, item.getSoDienThoai());
                        pstmt.setString(8, item.getEmail());
                        pstmt.setInt(9, item.getSoDu());
                        pstmt.setString(10, item.getLoaiTV());

                    }
                });

    }

    //update info thanh vien
    public int update(ThanhVien item) {
        return JdbcTemplate.update(
                "UPDATE ThanhVien SET HoTen = ?, NgaySinh = ?, GioiTinh = ?, DiaChi = ?, SoDienThoai = ?, Email = ? WHERE TenDangNhap = ?", new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1, item.getHoTen());
                        pstmt.setObject(2, item.getNgaySinh());
                        pstmt.setInt(3, item.getGioiTinh());
                        pstmt.setString(4, item.getDiaChi());
                        pstmt.setString(5, item.getSoDienThoai());
                        pstmt.setString(6, item.getEmail());
                        pstmt.setString(7, item.getTenDangNhap());

                    }
                });

    }

    //thay doi password
    public int updatePassword(String user, String password) {
        return JdbcTemplate.update(
                "UPDATE ThanhVien SET Matkhau WHERE TenDangNhap = ?", new JdbcTemplate.PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pstmt) throws SQLException {
                        pstmt.setString(1, password);
                        pstmt.setString(2, user);
                    }
                });

    }


    private JdbcTemplate.RowCallBackHandler<ThanhVien> callBackHandler() {
        return new JdbcTemplate.RowCallBackHandler<ThanhVien>() {
            @Override
            public ThanhVien processRow(ResultSet rs) throws SQLException {
                ThanhVien thanhVien = new ThanhVien();
                thanhVien.setDiaChi(rs.getNString(DIA_CHI));
                thanhVien.setEmail(rs.getString(EMAIL));
                thanhVien.setGioiTinh(rs.getInt(GIOI_TINH));
                thanhVien.setHoTen(rs.getNString(HO_TEN));
                thanhVien.setLoaiTV(rs.getString(LOAI_THANH_VIEN));
                thanhVien.setMatKhau(rs.getString(MAT_KHAU));
                thanhVien.setNgaySinh(rs.getObject(NGAY_SINH, LocalDate.class));
                thanhVien.setSoDienThoai(rs.getString(SO_DIEN_THOAI));
                thanhVien.setSoDu(rs.getInt(SO_DU));
                thanhVien.setTenDangNhap(rs.getString(TEN_DANG_NHAP));
                return thanhVien;
            }
        };
    }

}
