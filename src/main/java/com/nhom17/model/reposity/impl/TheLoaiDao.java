package com.nhom17.model.reposity.impl;

import com.nhom17.model.dto.Phim;
import com.nhom17.model.dto.TheLoai;
import com.nhom17.util.JdbcTemplate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TheLoaiDao extends CommonDao<TheLoai> {

    public static final String ID = "id";
    public static final String MA_THE_LOAI = "MaTheLoai";
    public static final String TEN_THE_LOAI = "TenTheLoai";

    private TheLoaiDao() {
    }

    public static TheLoaiDao createTheLoaiReposity() {
        return new TheLoaiDao();
    }


    @Override
    public List<TheLoai> getall() {
        return JdbcTemplate.query("SELECT * FROM [dbo].[TheLoai]", createHandler());
    }

    public List<TheLoai> getByMovie(final Phim phim) {
        return JdbcTemplate.query("SELECT tl.* FROM TheLoaiPhim tlp, TheLoai tl WHERE tl.MaTheLoai = tlp.MaTheLoai AND tlp.MaPhim = ?", new JdbcTemplate.PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, phim.getMaPhim());
            }
        }, createHandler());
    }

    private JdbcTemplate.RowCallBackHandler<TheLoai> createHandler() {
        return new JdbcTemplate.RowCallBackHandler<TheLoai>() {
            @Override
            public TheLoai processRow(ResultSet rs) throws SQLException {
                TheLoai theLoai = new TheLoai();
                theLoai.setId(rs.getInt(ID));
                theLoai.setTenTheLoai(rs.getString(TEN_THE_LOAI));
                theLoai.setMaTheLoai(rs.getString(MA_THE_LOAI));

                return theLoai;
            }
        };
    }
}
