package com.nhom17.model.dto;

import java.time.LocalDate;

public class XuatChieu {
    private String maXuatChieu;
    private String tenPhim;
    private String thoiGianChieu;
    private LocalDate ngayChieu;
    private String maPhim;
    private String maPhong;
    private int id;
    private  String maDangPhim;

    public XuatChieu(){

    }
    public String getMaXuatChieu(){
        return maXuatChieu;
    }

    public void setMaXuatChieu(String maXuatChieu) {
        this.maXuatChieu = maXuatChieu;
    }

    public String getThoiGianChieu() {
        return thoiGianChieu;
    }

    public void setThoiGianChieu(String thoiGianChieu) {
        this.thoiGianChieu = thoiGianChieu;
    }

    public LocalDate getNgayChieu() {
        return ngayChieu;
    }

    public void setNgayChieu(LocalDate ngayChieu) {
        this.ngayChieu = ngayChieu;
    }

    public String getMaPhim() {
        return maPhim;
    }

    public void setMaPhim(String maPhim) {
        this.maPhim = maPhim;
    }

    public String getMaPhong() {
        return maPhong;
    }

    public void setMaPhong(String maPhong) {
        this.maPhong = maPhong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaDangPhim() {
        return maDangPhim;
    }

    public void setMaDangPhim(String maDangPhim) {
        this.maDangPhim = maDangPhim;
    }

    public String getTenPhim() {
        return tenPhim;
    }

    public void setTenPhim(String tenPhim) {
        this.tenPhim = tenPhim;
    }
}
