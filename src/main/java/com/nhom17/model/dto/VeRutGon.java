package dto;

public class VeRutGon {
    private String maXuatChieu;
    private int soGheChuaBan;
    private int soGheDaBan;
    private int id;
    private int tongThu;
    private String maLoaiGhe;

    public VeRutGon() {

    }

    public String getMaXuatChieu() {
        return maXuatChieu;
    }

    public void setMaXuatChieu(String maXuatChieu) {
        this.maXuatChieu = maXuatChieu;
    }

    public int getSoGheChuaBan() {
        return soGheChuaBan;
    }

    public void setSoGheChuaBan(int soGheChuaBan) {
        this.soGheChuaBan = soGheChuaBan;
    }

    public int getSoGheDaBan() {
        return soGheDaBan;
    }

    public void setSoGheDaBan(int soGheDaBan) {
        this.soGheDaBan = soGheDaBan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTongThu() {
        return tongThu;
    }

    public void setTongThu(int tongThu) {
        this.tongThu = tongThu;
    }

    public String getMaLoaiGhe() {
        return maLoaiGhe;
    }

    public void setMaLoaiGhe(String maLoaiGhe) {
        this.maLoaiGhe = maLoaiGhe;
    }
}
