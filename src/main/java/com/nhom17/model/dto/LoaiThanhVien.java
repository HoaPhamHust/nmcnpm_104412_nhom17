package com.nhom17.model.dto;

public class LoaiThanhVien {
    private String maLoaiThanhVien;
    private String tenLoaiThanhVien;

    public  LoaiThanhVien() {

    }

    public String getMaLoaiThanhVien() {
        return maLoaiThanhVien;
    }

    public void setMaLoaiThanhVien(String maLoaiThanhVien) {
        this.maLoaiThanhVien = maLoaiThanhVien;
    }

    public String getTenLoaiThanhVien() {
        return tenLoaiThanhVien;
    }

    public void setTenLoaiThanhVien(String tenLoaiThanhVien) {
        this.tenLoaiThanhVien = tenLoaiThanhVien;
    }
}
