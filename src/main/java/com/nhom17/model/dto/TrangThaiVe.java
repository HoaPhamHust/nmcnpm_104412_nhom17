package com.nhom17.model.dto;

public class TrangThaiVe {
    private int maTrangThaiVe;
    private String tenTrangThaiVe;

    public TrangThaiVe() {

    }

    public int getMaTrangThaiVe() {
        return maTrangThaiVe;
    }

    public void setMaTrangThaiVe(int maTrangThaiVe) {
        this.maTrangThaiVe = maTrangThaiVe;
    }

    public String getTenTrangThaiVe() {
        return tenTrangThaiVe;
    }

    public void setTenTrangThaiVe(String tenTrangThaiVe) {
        this.tenTrangThaiVe = tenTrangThaiVe;
    }
}
