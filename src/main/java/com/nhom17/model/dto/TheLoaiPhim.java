package com.nhom17.model.dto;

public class TheLoaiPhim {
    private String maPhim;
    private String maTheLoai;

    public TheLoaiPhim() {

    }

    public String getMaPhim() {
        return maPhim;
    }

    public void setMaPhim(String maPhim) {
        this.maPhim = maPhim;
    }

    public String getMaTheLoai() {
        return maTheLoai;
    }

    public void setMaTheLoai(String maTheLoai) {
        this.maTheLoai = maTheLoai;
    }
}
