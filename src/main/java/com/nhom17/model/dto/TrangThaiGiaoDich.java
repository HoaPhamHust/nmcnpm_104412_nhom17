package com.nhom17.model.dto;

public class TrangThaiGiaoDich {
    private String trangThaiGiaoDich;
    private int id;

    public TrangThaiGiaoDich() {

    }

    public String getTrangThaiGiaoDich() {
        return trangThaiGiaoDich;
    }

    public void setTrangThaiGiaoDich(String trangThaiGiaoDich) {
        this.trangThaiGiaoDich = trangThaiGiaoDich;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
