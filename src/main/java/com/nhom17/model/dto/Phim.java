package com.nhom17.model.dto;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.time.LocalDate;

public class Phim {
    private String maPhim;
    private String imdbURL;
    private LocalTime thoiLuongPhim;
    private String nhanPhim;
    private String posterURL;
    private String motaPhim;
    private String tenPhim;
    private float imdbRank;
    private LocalDate ngayBatDau;
    private int id;
    private LocalDate ngayKetThuc;
    private String ghiChu;
    private String quocGia;
    private String daodien;
    private String dienVien;
    private String poster2URL;
    private List<TheLoai> theLoaiList;

    public Phim() {

    }

    public String getMaPhim() {
        return maPhim;
    }

    public void setMaPhim(String maPhim) {
        this.maPhim = maPhim;
    }

    public String getImdbURL() {
        return imdbURL;
    }

    public void setImdbURL(String imdbURL) {
        this.imdbURL = imdbURL;
    }

    public LocalTime getThoiLuongPhim() {
        return thoiLuongPhim;
    }

    public void setThoiLuongPhim(LocalTime thoiLuongPhim) {
        this.thoiLuongPhim = thoiLuongPhim;
    }

    public String getNhanPhim() {
        return nhanPhim;
    }

    public void setNhanPhim(String nhanPhim) {
        this.nhanPhim = nhanPhim;
    }

    public String getPosterURL() {
        return posterURL;
    }

    public void setPosterURL(String c){
        this.posterURL = c;
    }

    public String getMotaPhim() {
        return motaPhim;
    }

    public void setMotaPhim(String motaPhim) {
        this.motaPhim = motaPhim;
    }

    public String getTenPhim() {
        return tenPhim;
    }

    public void setTenPhim(String tenPhim) {
        this.tenPhim = tenPhim;
    }

    public float getImdbRank() {
        return imdbRank;
    }

    public void setImdbRank(float imdbRank) {
        this.imdbRank = imdbRank;
    }

    public LocalDate getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(LocalDate ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(LocalDate ngayKetThuc) {
        this.ngayKetThuc = ngayKetThuc;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getQuocGia() {
        return quocGia;
    }

    public void setQuocGia(String quocGia) {
        this.quocGia = quocGia;
    }

    public String getDaodien() {
        return daodien;
    }

    public void setDaodien(String daodien) {
        this.daodien = daodien;
    }

    public String getDienVien() {
        return dienVien;
    }

    public void setDienVien(String dienVien) {
        this.dienVien = dienVien;
    }

    public String getPoster2URL() {
        return poster2URL;
    }

    public void setPoster2URL(String poster2URL) {
        this.poster2URL = poster2URL;
    }

    public List<TheLoai> getTheLoaiList() {
        return theLoaiList;
    }

    public void setTheLoaiList(List<TheLoai> theLoaiList) {
        this.theLoaiList = theLoaiList;
    }
}
