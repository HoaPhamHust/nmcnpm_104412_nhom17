package com.nhom17.model.dto;

public class Ve {
    private String maVe;
    private String maGia;
    private String maXuatChieu;
    private String maGhe;
    private int maTrangThaiVe;

    public Ve() {

    }

    public String getMaVe() {
        return maVe;
    }

    public void setMaVe(String maVe) {
        this.maVe = maVe;
    }

    public String getMaGia() {
        return maGia;
    }

    public void setMaGia(String maGia) {
        this.maGia = maGia;
    }

    public String getMaXuatChieu() {
        return maXuatChieu;
    }

    public void setMaXuatChieu(String maXuatChieu) {
        this.maXuatChieu = maXuatChieu;
    }

    public String getMaGhe() {
        return maGhe;
    }

    public void setMaGhe(String maGhe) {
        this.maGhe = maGhe;
    }

    public int getMaTrangThaiVe() {
        return maTrangThaiVe;
    }

    public void setMaTrangThaiVe(int maTrangThaiVe) {
        this.maTrangThaiVe = maTrangThaiVe;
    }
}
