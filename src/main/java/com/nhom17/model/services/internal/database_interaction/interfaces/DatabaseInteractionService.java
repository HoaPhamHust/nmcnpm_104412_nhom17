package com.nhom17.model.services.internal.database_interaction.interfaces;

public interface DatabaseInteractionService {

	public boolean openService();

	public boolean closeService();
}
